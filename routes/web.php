<?php

use App\Http\Controllers\regCont;
use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/index', [regCont::class,'register']);

Route::post('/simpan', [regCont::class,'prosesSimpan']);

Route::get('/delete/{id_karyawan}',[regCont::class,'delete']);

Route::get('/edit/{id_karyawan}',[regCont::class,'edit']);

Route::post('/update/{id_karyawan}',[regCont::class,'update']);