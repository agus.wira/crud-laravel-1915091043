<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class regCont extends Controller
{
    public function register(){
        $user = DB::table('karyawan')->get();

        return view('/index',['karyawan'=>$user]);

    }


    public function prosesSimpan(Request $req){
        $nama_karyawan = $req->nama_karyawan;
        $no_karyawan = $req->no_karyawan;
        $no_telp_karyawan = $req->no_telp_karyawan;
        $jabatan_karyawan = $req->jabatan_karyawan;
        $divisi_karyawan = $req->divisi_karyawan;

        DB::table('karyawan')->insert(
            ['nama_karyawan'=>$nama_karyawan,
            'no_karyawan'=>$no_karyawan, 
            'no_telp_karyawan'=>$no_telp_karyawan,
            'jabatan_karyawan'=>$jabatan_karyawan,
            'divisi_karyawan'=>$divisi_karyawan]
    );

    return redirect('/index');
    }

    public function delete($id_karyawan){
        DB::table('karyawan')->where('id_karyawan', $id_karyawan)->delete();
        return redirect('/index');
    }

    public function edit($id_karyawan)
    {
        $user = DB::table('karyawan')->where('id_karyawan', $id_karyawan)->get();
        return view('/edit', ['karyawan' => $user]);
    }

    public function update (Request $req)
    {
        DB::table('karyawan')->where('id_karyawan', $req->id_karyawan)->update
        (['nama_karyawan' => $req->nama_karyawan,
        'no_karyawan' => $req->no_karyawan,
        'no_telp_karyawan' => $req->no_telp_karyawan,
        'jabatan_karyawan' => $req->jabatan_karyawan,
        'divisi_karyawan' => $req->divisi_karyawan,]);

        return redirect('/index');
    }
}



