<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    
    @foreach ($karyawan as $s)    
    <form action="/update/{{ $s->id_karyawan }}" method="POST">
        {{ csrf_field() }}
        <div class="container">
            <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">{{$s->nama_karyawan}}</label>
            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="nama_karyawan">  
            </div>

            <div class="mb-3">  
                <label for="exampleInputEmail1" class="form-label">{{$s->no_karyawan}}</label>
                <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="no_karyawan">
              </div>    

          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">{{$s->no_telp_karyawan}}</label>
            <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="no_telp_karyawan">
          </div>

          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">{{$s->jabatan_karyawan}}</label>
            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="jabatan_karyawan">
          </div>

          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">{{$s->divisi_karyawan}}</label>
            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="divisi_karyawan">
          </div>
    
      
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
      @endforeach
    </div>
    


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

   
  </body>
</html>