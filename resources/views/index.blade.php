<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>

    <form action="/simpan" method="POST">
        {{ csrf_field() }}
        <div class="container">
            <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">nama karyawan</label>
            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="nama_karyawan">  
            </div>

            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">no karyawan</label>
                <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="no_karyawan">
              </div>    

          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">no telpon</label>
            <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="no_telp_karyawan">
          </div>

          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">jabatan karyawan</label>
            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="jabatan_karyawan">
          </div>

          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">divisi karyawan</label>
            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="divisi_karyawan">
          </div>
    
      
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>




    <div class="container" style="margin-top:50px;">
        <?php $i=1;?>
      <div class="align-self-center " >
          <table class="table table-primary table-bordered table-striped">
              <thead>
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">No Karyawan</th>
                    <th scope="col">No Telepon</th>
                    <th scope="col">Jabatan</th>
                    <th scope="col">Divisi</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
            @foreach ($karyawan as $s)
              <tbody>
                <tr>
                  <td><?php echo($i); $i++; ?></td>
                  <th scope="col">{{ $s->nama_karyawan }}</th>
                  <th scope="col">{{ $s->no_karyawan }}</th>
                  <th scope="col">{{ $s->no_telp_karyawan }}</th>
                  <th scope="col">{{ $s->jabatan_karyawan }}</th>
                  <th scope="col">{{ $s->divisi_karyawan }}</th>
                  <th scope="col">
                    <a href="/edit/{{ $s->id_karyawan }}"><button type="button" class="btn btn-success ">Edit</button></a>
                    <a href="/delete/{{ $s->id_karyawan }}"><button type="button" class="btn btn-danger ">Delete</button></a>
                </tr>
              </tbody>
              @endforeach
            </table>
      </div>
      </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

   
  </body>
</html>